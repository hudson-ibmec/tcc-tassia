##############################################
#########         PACOTES              #######
##############################################

# install.packages(c("tidyquant","tidyverse","forecast", "magrittr",
# "MTS","vars","urca","seasonal","rprojroot", "deflateBR", "tbl2xts",
# "Quandl", "BETS", "ecoseries", "texreg"))

# Pacotes para coletar os dados
suppressMessages(require(tidyquant))
suppressMessages(require(Quandl))
suppressMessages(require(BETS))
suppressMessages(require(ecoseries))
suppressMessages(require(quantmod))

# Pacotes para manusear os dados
suppressMessages(require(tidyverse))
suppressMessages(require(tbl2xts))
suppressMessages(require(readxl))

# Pacote para facilitar o uso de arquivos do projeto
suppressMessages(require(rprojroot))

# Pacote para estimar o modelo var
suppressMessages(require(vars))
suppressMessages(require(urca))
suppressMessages(require(MTS))
suppressMessages(require(forecast))
suppressMessages(require(seasonal))

# Pacote para gerar os gráficos
suppressMessages(require(dygraphs))

# Pacote para deflacionar séries
suppressMessages(require(deflateBR))

# Pacote para gerar tabelas dos modelos estimados
suppressMessages(require(texreg))

##############################################
#########         OPÇÕES               #######
##############################################

root = rprojroot::is_rstudio_project 
mydir = root$find_file()   # Encontra o diretório no qual o projeto atual está salvo

##############################################
#########         DADOS                #######
##############################################

####
## DADOS COLETADOS AUTOMATICAMENTE PELO R
####

from = "2002-01-01"
type = "xts"
quandl_api_key("NjGc22-41R7zD_K7Pt7z")

# Índice IBOVESPA - Mensal e em pontos (BCB) - Obtemos a taxa de variação mensal
bvsp = Quandl::Quandl("BCB/7845", type = type, start_date = from, transform = "rdiff")
colnames(bvsp) = "bvsp"

# PIB acumulado dos últimos 12 meses - Mensal e em valores correntes (R$ milhões) (BCB) - Obtemos a taxa de variação mensal
pibacum12m = Quandl::Quandl("BCB/4382", type = type, start_date = from) %>%
  tbl2xts::xts_tbl() %>%
  dplyr::select(date, pibacum12m = "coredata.xts.") %>%
  dplyr::mutate(date = as.Date(date)) %>%
  dplyr::mutate(pib_deflacionado = deflateBR::deflate(pibacum12m, date, "03/2011", index = "ipca")) %>%
  dplyr::mutate(pibvar = (pib_deflacionado/lag(pib_deflacionado)-1)*100) %>%
  dplyr::select(date, pibvar) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "pibvar")

# Taxa média mensal de juros - Pessoas físicas - Financiamento imobiliário com taxas de mercado
# % a.m. 
juros_imobiliarios = BETSget(25497, data.frame = T) %>%
  dplyr::as.tbl() %>%
  dplyr::select(date, juros_imobiliarios = value) %>%
  dplyr::mutate(jurosvar = (juros_imobiliarios/lag(juros_imobiliarios)-1)*100) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "jurosvar")


juros_imobiliarios = juros_imobiliarios[paste0(from,"/")] # selecionar para o período de interesse

# Operações de crédito com recursos livres referenciais para taxa de juros (pós-fixada)
# Financiamento imobiliário - Pessoa física - Taxa total - Até dez 2012
# % a.d. - Mensal - Transformamos para uma taxa mensal
juros_imobiliarios_two = Quandl("BCB/943", type = type, start_date = from) %>%
                          tbl2xts::xts_tbl() %>%
                          dplyr::mutate(monthly_rate = (1+coredata.xts.)^30-1) %>%
                          dplyr::mutate(jurosvar1 = (monthly_rate/lag(monthly_rate)-1)*100) %>%
                          dplyr::select(date, jurosvar1) %>%
                          tbl2xts::tbl_xts(., cols_to_xts = "jurosvar1")

# Juntar as duas séries de juros
juros_imobiliarios_filter = juros_imobiliarios_two["/2011-03-01"]
final_juros = merge.xts(juros_imobiliarios_filter,juros_imobiliarios) %>%
              tbl2xts::xts_tbl() %>%
              dplyr::mutate(juros_final = ifelse(is.na(jurosvar1),jurosvar,jurosvar1)) %>%
              dplyr::select(date, juros_final) %>%
              tbl2xts::tbl_xts(., cols_to_xts = "juros_final")

# juros_imobiliarios2 = BETSget(25498, data.frame = T) %>%
#   dplyr::as.tbl() %>%
#   dplyr::select(date, juros_imobiliarios2 = value) %>%
#   dplyr::mutate(jurosvar2 = (juros_imobiliarios2/lag(juros_imobiliarios2)-1)*100) %>%
#   tbl2xts::tbl_xts(., cols_to_xts = "jurosvar2")
# 
# juros_imobiliarios2 = juros_imobiliarios2[paste0(from,"/")] # selecionar para o período de interesse
# 
# juros_imobiliarios3 = BETSget(25499, data.frame = T) %>%
#   dplyr::as.tbl() %>%
#   dplyr::select(date, juros_imobiliarios3 = value) %>%
#   dplyr::mutate(jurosvar3 = (juros_imobiliarios3/lag(juros_imobiliarios3)-1)*100) %>%
#   tbl2xts::tbl_xts(., cols_to_xts = "jurosvar3")
# 
# juros_imobiliarios3 = juros_imobiliarios3[paste0(from,"/")] # selecionar para o período de interesse

# Índice de valores de garantia de imóveis residenciais financiados (IVG-R) - Em índice
# Estima a tendência de preço de longo prazo dos valores de imóveis residenciais no Brasil
# utilizando informações do Sistema de Informações de Crédito (SCR) do Banco Central do Brasil. 
# Para isso, utiliza os valores de avaliação dos imóveis dados em garantia a financiamentos 
# imobiliários residenciais para pessoas físicas nas modalidades de alienação fiduciária e 
# hipoteca residencial. O cálculo é realizado considerando as mesmas regiões metropolitanas 
# usadas no cálculo do IPCA pelo IBGE: 11 até dezembro de 2013 (Belém, Belo Horizonte, Brasília, 
# Curitiba, Fortaleza, Goiânia, Porto Alegre, Recife, Rio de Janeiro, Salvador e São Paulo), 
# e 13 a partir de janeiro de 2014 (inclui Campo Grande e Vitória, conforme NT/IBGE 03/2013).

# ivgr = Quandl::Quandl("BCB/21340", type = type, start_date = from) %>%
#   tbl2xts::xts_tbl() %>%
#   dplyr::select(date, ivgr = "coredata.xts.") %>%
#   dplyr::mutate(date = as.Date(date)) %>%
#   dplyr::mutate(ivgrvar = (ivgr/lag(ivgr)-1)*100) %>%
#   tbl2xts::tbl_xts(., cols_to_xts = "ivgrvar")

# Mediana dos valores de garantia de imóveis residenciais financiados (MVG-R) - em R$ 
# É uma série temporal complementar ao IVG-R (série 21340). É calculada a partir dos 
# valores de avaliação dos imóveis dados em garantia a financiamentos imobiliários 
# residenciais para pessoas físicas nas modalidades de alienação fiduciária e 
# hipoteca residencial, informados pelos bancos ao Sistema de Informações de Crédito (SCR) 
# do Banco Central do Brasil. Seu cálculo consiste em partir da mesma base de dados 
# para o cálculo do IVG-R e tomar a mediana dos valores de avaliação dos imóveis em uma 
# janela móvel de três meses. A abrangência é nacional e o valor é expresso em reais. 
# Considera como filtro de dados apenas financiamentos com valor contratado superior 
# a um salário mínimo anual, dado como 13,3 vezes o salário mínimo mensal vigente no ano.

mvgr = BETSget(25419, data.frame = T) %>%
  dplyr::as.tbl() %>%
  dplyr::select(date, precos_mvgr = value) %>%
  dplyr::mutate(precos_mvgr_var = (precos_mvgr/lag(precos_mvgr)-1)*100) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "precos_mvgr_var")

mvgr = mvgr[paste0(from,"/")] # selecionar para o período de interesse

##############################################
#######    EXPLORATORY DATA ANALYSIS   #######
##############################################

# união dos dados em um único objeto
data = xts::merge.xts(bvsp, pibacum12m, final_juros, mvgr)
data = na.omit(data)
data_names = colnames(data)

# Estatística descritiva das séries 
statNames = c("Média", "Desvio Padrão", "Assimetria", "Curtose", "Tamanho")
series_stats_data = matrix(data = NA, nrow = length(data_names), ncol = length(statNames))
colnames(series_stats_data) = statNames

for (i in 1:nrow(series_stats_data)) {
  series_stats_data[i,1] = round(mean(data[,i], na.rm = TRUE),3)
  series_stats_data[i,2] = round(sd(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,3] = round(skewness(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,4] = round(kurtosis(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,5] = length(na.omit(data[,i]))
}

# Mostrar a tabela com as estatísticas descritivas
print(series_stats_data)

##############################################
#######          MODELO VAR            #######
##############################################

#####
##   PROCESSO DE ESTIMAÇÃO
#####

# 1. Visualizar os dados e identificar observações fora do padrão (outliers, sazonalidade, tendência)
# 2. Se necessário, transformar os dados para estabilizar a variância (logaritmo ou retirar sazonalidade, por exemplo)
# 3. Avaliar a função de correlação cruzada para confirmar a possibilidade de modelagem multivariada.
# 4. Testar se os dados são estacionários. Caso tenha raiz unitária é preciso diferenciar os dados até se tornarem estacionários
# 5. Definir a ordem $p$ para os dados em análise por meio de critérios de informação (escolher modelo com menor AIC, por exemplo)
# 6. Estimar o modelo escolhido no passo anterior. 
# 7. Verificar significância estatística do modelo estimado e, caso seja necessário, eliminar parâmetros não significantes.
#     * Para tanto, uma alternativa é usar o teste de causalidade de Granger
# 8. Examinar se os resíduos se comportam como ruído branco e condições de estacionariedade do modelo. Caso contrário, retornar ao passo 3 ou 4.
#     * Verificar a autocorrelação serial por meio da FAC e FACP dos resíduos de cada equação do modelo estimado. O ideal é não ter defasagens significativas.
#     * Verificar correlação cruzada por meio da FCC dos resíduos.
#     * Analisar a estabildiade do modelo estimado através dos autovalores associados ao mesmo.
# 9. Uma vez que os resíduos são ruído branco e o modelo é estável:
#     * Analisar funções de resposta ao impulso
#     * Analisar a importância das variáveis para explicar a variância do erro de previsão de cada variável
#     * Fazer previsões paras as variáveis do modelo


#####
##   1. Visualizar os dados e identificar observações fora do padrão (outliers, sazonalidade, tendência).
#####

# Gráfico das séries temporais - agrupado com 4 séries por tipo
par(mfrow=c(2,2))
plot(data$bvsp, xlab = "", ylab = "", type = "l", main = "Taxa de variação do IBOVESPA - %", cex = 0.5)
plot(data$pibvar, xlab = "", ylab = "", type = "l", main = "Taxa de variação do PIB Real - %", cex = 0.5)
plot(data$juros_final, xlab = "", ylab = "", type = "l", main = "Taxa variação dos Juros Mercado - %", cex = 0.5)
plot(data$precos_mvgr_var, xlab = "", ylab = "", type = "l", main = "Taxa de variação MVG - %", cex = 0.5)

#####
##   2. Se necessário, transformar os dados para estabilizar a variância (logaritmo ou retirar sazonalidade, por exemplo)
#####


#####
##  3. Avaliar a função de correlação cruzada para confirmar a possibilidade de modelagem multivariada. 
#####

# Defasagens máximas
defasagens = 10

# Em função do tamanho do gráfico criamos um arquivo pdf que vai armazenar os gráficos 
pdf("/cloud/project/figures/fcc_data.pdf", paper = "USr", width = 10, height = 10)

# Montar uma "matriz" de gráficos (kxk)
par(mfrow = c(ncol(data), ncol(data)))

# Adicionar na "matriz" de gráficos a CCF de cada série
# contra ela mesma e as demais
for (i in 1:ncol(data)) {
  for (j in 1:ncol(data)) {
    ccf(drop(data[,i]), drop(data[,j]), lag.max = defasagens, 
        main = "", ylab = "FCC", xlab = "Defasagens")
    title(paste0(colnames(data)[i], "-", colnames(data)[j]), adj = 0.4, line = 1)
  }
}

# parar de gravar no arquivo pdf criado
dev.off()

# Os gráficos mostram a dependência linear entre as séries temporais, principalmente entre variações nos preços 
# dos imóveis e variação no pib. Assim, faz sentido usar um modelo multivariado para avaliar a dependência 
# entre as séries.

#####
##  4. Testar se os dados são estacionários. Caso tenha raiz unitária é preciso diferenciar os dados até se tornarem estacionários
#####

# Aqui, usamos a função adfTest do pacote fUnitRoots para testar se há raiz unitária
# na séries temporais avaliadas. Como observamos no gráfico da série do agregado monetário, 
# há tendência nos dados e assim o teste verificará para esta série se a série se comporta 
# como um passeio aleatório com drift. Para as demais, o teste verificará se a série se
# comporta como um passeio aleatório sem drift. 
# Isto é evidênciado por meio da opção type que tem as seguintes opções:
# - nc: for a regression with no intercept (constant) nor time trend (passeio aleatório)
# - c: for a regression with an intercept (constant) but no time trend (passeio aleatório com drift)
# - ct: for a regression with an intercept (constant) and a time trend (passeio aleatório com constante e tendência)
# Além disso, definimos que no máximo três defasagens da série devem ser usadas como
# variáveis explicativas da regressão do teste.
unitRootbvsp = fUnitRoots::adfTest(data[,1],lags=3, type=c("nc"))
unitRootpibacum12m = fUnitRoots::adfTest(data[,2],lags=3, type=c("nc"))
unitRootjuros_imobiliarios = fUnitRoots::adfTest(data[,3],lags=3, type=c("nc"))
unitRootmvgr = fUnitRoots::adfTest(data[,4],lags=3, type=c("nc"))

# Tabela com os resultados do teste
adfbvsp = c(unitRootbvsp@test$statistic, unitRootbvsp@test$p.value)
adfpibacum12m = c(unitRootpibacum12m@test$statistic, unitRootpibacum12m@test$p.value)
adfjuros_imobiliarios = c(unitRootjuros_imobiliarios@test$statistic, unitRootjuros_imobiliarios@test$p.value)
adfmvgr= c(unitRootmvgr@test$statistic, unitRootmvgr@test$p.value)
resultADF = cbind(adfbvsp, adfpibacum12m, adfjuros_imobiliarios, adfmvgr)
colnames(resultADF) = c("bvsp", "pibacum12m", "juros_imobiliarios", "mvgr")
rownames(resultADF) = c("Estatística do Teste ADF", "p-valor")
print(resultADF)

#####
##  5. Definir a ordem p para os dados em análise por meio de critérios de informação (escolher modelo com menor AIC, por exemplo)
#####

# Usamos a função VARselect do pacote vars que tem as seguintes opções:
# - y: dados do modelo
# - lag.max: quantidade máxima de defasagens avaliadas. Aqui, lembre-se que
# para cada defasagem um modelo VAR será estimado 
# - type: quais parâmetros determinísticos queremos incluir no modelo. Eles podem ser:
#      - "const" para uma constante nas equações
#      - "trend" uma tendência nas equações
#      - "both" para tando constante quanto tendência
#      - "none": nenhum dos dois (apenas parâmetros das defasagens)
# - season: dummies sazonais caso os dados apresentem sazonalidade
# - exogen: variáveis exógenas do modelo (dummies, por exemplo)
varorder = vars::VARselect(y = data, lag.max = 6, type = "const")
print(varorder$criteria)


#####
##  6. Estimar o modelo escolhido no passo anterior.
#####

# Usamos a função VAR do pacote vars que tem as seguintes opções além das opções já apresentadas em VARselect:
# - p: número de defasagens do modelo
modelo = vars::VAR(y = data, p = 1, type = "const")

# Gerar tabela com resultados
texreg(list(modelo$varresult$bvsp, modelo$varresult$pibvar, 
            modelo$varresult$juros_final, modelo$varresult$precos_mvgr_var), use.packages=FALSE, single.row=TRUE)

#####
##  7. Verificar significância estatística do modelo estimado e, caso seja necessário, eliminar parâmetros não significantes.
#####

# A partir dos resultados obtidos no passo anterior, percebemos que há parâmetros não significantes estatisticamente.
# Em função disso, vamos restringir nosso modelo VAR(2) eliminando variáveis ou parâmetros que prejudicam o ajuste do
# modelo. Para tanto, vamos executar o teste de causalidade de Granger para confirmar ou não a relação entre as variáveis
# e justificar a inclusão delas no VAR(2)

# Aqui, usamos a função causality do pacote vars para executar
# os testes. Temos as seguintes opções:
# - x: modelo VAR estimado
# - causa: a variável de "causa"
# - vcov: permite especificar manualmente a matriz de covariância
vars::causality(modelo, cause = "bvsp")$Granger
vars::causality(modelo, cause = "pibvar")$Granger
vars::causality(modelo, cause = "juros_final")$Granger
vars::causality(modelo, cause = "precos_mvgr_var")$Granger

# Os resultados do teste de causalidade de Granger mostram que parece que não há causalidade no sentido de Granger
# entre algumas variáveis. Apesar disso, vamos deixar as variáveis no modelo e restrigi-lo de forma que apenas 
# variáveis que são estatisticamente significantes ao nível de 10% de significância (representa 1.645)
# modelo restrito com variáveis que são estatisticamente significante ao nível de 5%
var.restricted = vars::restrict(modelo, method = "ser", thresh = 1.645)

# tabela com os resultados do modelo restrito
texreg(list(var.restricted$varresult$bvsp, var.restricted$varresult$pibvar, 
            var.restricted$varresult$juros_final, var.restricted$varresult$precos_mvgr_var), use.packages=FALSE, single.row=TRUE)

####
## ANÁLISE DOS RESÍDUOS E HIPÓTESES DO MODELO
####

#####
##  8. Examinar se os resíduos se comportam como ruído branco e condições de estacionariedade do modelo. Caso contrário, retornar ao passo 3 ou 4.
#####

# Gráficos para avaliação do ajuste da primeira equação do modelo
pdf("/cloud/project/figures/residuals_bvsp.pdf", paper = "USr", width = 10, height = 10)
plot(var.restricted, names = "bvsp", lag.acf = 16, lag.pacf = 16)
dev.off()

# Gráficos para avaliação do ajuste da segunda equação do modelo
pdf("/cloud/project/figures/residuals_pibvar.pdf", paper = "USr", width = 10, height = 10)
plot(modelo, names = "pibvar", lag.acf = 16, lag.pacf = 16)
dev.off()

# Gráficos para avaliação do ajuste da terceira equação do modelo
pdf("/cloud/project/figures/residuals_juros_final.pdf", paper = "USr", width = 10, height = 10)
plot(modelo, names = "juros_final", lag.acf = 16, lag.pacf = 16)
dev.off()

# Gráficos para avaliação do ajuste da quarta equação do modelo
pdf("/cloud/project/figures/residuals_precos_mvgr_var.pdf", paper = "USr", width = 10, height = 10)
plot(modelo, names = "precos_mvgr_var", lag.acf = 16, lag.pacf = 16)
dev.off()

# Correlação Cruzada dos Resíduos

# Defasagens máximas
defasagens = 10

# Em função do tamanho do gráfico criamos um arquivo pdf que vai armazenar os gráficos 
pdf("/cloud/project/figures/cross_correlation_residuals.pdf", paper = "USr", width = 10, height = 10)

# Montar uma "matriz" de gráficos (kxk)
par(mfrow = c(ncol(data), ncol(data)))

# Adicionar na "matriz" de gráficos a CCF de cada série contra ela mesma e as demais
for (i in 1:ncol(data)) {
  for (j in 1:ncol(data)) {
    ccf(var.restricted$varresult[[i]]$residuals, var.restricted$varresult[[j]]$residuals, lag.max = defasagens, 
        main = "", ylab = "FCC", xlab = "Defasagens")
    title(paste0(colnames(data)[i], "-", colnames(data)[j]), adj = 0.5, line = 1)
  }
}
dev.off()

# Como é possível observar, para os termos de erro de cada variável não temos resíduos com autocorrelação 
# (a partir da não significância estatística das defasagens das funções FAC e FACP). Já a correlação cruzada entre 
# os resíduos das quatro equações pode ser verificada pela Função de Correlação Cruzada (FCC).

# Agora, precisamos avaliar a estabilidade do modelo
# Aqui, usamos a função roots do pacote vars que recebe como opção:
# - x: modelo VAR estimado
# - modulus: retorna o valor absoluto das raízes. Caso contrário, 
# retorna tanto a parte real como a complexa, caso exista.
vars::roots(x = var.restricted, modulus = TRUE)

#####
##  9. Uma vez que os resíduos são ruído branco e o modelo é estável:
#####

###  Impulso resposta 

# Usamos a função irf do pacote vars para obter as 
# funções de impulso resposta do modelo. Tal função
# tem as seguintes opções:
# - x: modelo var que será analisado
# - impulse: o nome da variável que queremos impulsionar.
# O nome deve ser o mesmo da saída do modelo
# - response: variáveis que queremos obter a resposta. Aqui,
# podemos obter a resposta de uma única variável colocando
# seu nome ou deixando NULL que calculará a resposta em 
# todas as demais váriáveis do modelo
# - n.ahead: passos à frente que queremos visualizar
# - demais opções: veja help(irf)

# Resposta do choque em bvsp e a resposta em precos_mvgr_var
model1.irf = vars::irf(x = var.restricted, impulse = "bvsp", response = "precos_mvgr_var", n.ahead = 40, ortho = FALSE)

# Resposta do choque em pibvar e a resposta em precos_mvgr_var
model2.irf = vars::irf(var.restricted, impulse = "pibvar", response = "precos_mvgr_var", n.ahead = 40, ortho = FALSE)

# Resposta do choque em juros_final e a resposta em precos_mvgr_var
model3.irf = vars::irf(var.restricted, impulse = "juros_final", response = "precos_mvgr_var", n.ahead = 40, ortho = FALSE)

##########################
###  GRÁFICOS DA IR    ###
##########################

plot.ts(model1.irf$irf$bvsp[,1], axes=F, ylab='variação nos preços')
lines(model1.irf$Lower$bvsp[,1], lty=2, col='red')
lines(model1.irf$Upper$bvsp[,1], lty=2, col='red')
axis(side=2, las=2, ylab='')
abline(h=0, col='red')
box()
mtext("Resposta na variação dos preços dos imóveis dado choque na variação do bovespa")

plot.ts(model2.irf$irf$pibvar[,1], axes=F, ylab='variação nos preços')
lines(model2.irf$Lower$pibvar[,1], lty=2, col='red')
lines(model2.irf$Upper$pibvar[,1], lty=2, col='red')
axis(side=2,las=2, ylab='')
abline(h=0, col='red')
box()
mtext("Resposta na variação dos preços dos imóveis dado choque na variação do PIB")

plot.ts(model3.irf$irf$juros_final[,1], axes=F, ylab='variação nos preços')
lines(model3.irf$Lower$juros_final[,1], lty=2, col='red')
lines(model3.irf$Upper$juros_final[,1], lty=2, col='red')
axis(side=1, las=1)
axis(side=2, las=2)
abline(h=0, col='red')
box()
mtext("Resposta na variação dos preços dos imóveis dado choque na variação dos juros de financiamento")


# Analisar a importância das variáveis para explicar a variância do erro de previsão de cada variável
# Usamos a função fevd do pacote vars que tem as opções
# - x: modelo a ser analisado
# - n.ahead: horizonte de previsão de interesse
fevd.model = vars::fevd(x = var.restricted, n.ahead = 5)

# Em função do tamanho do gráfico criamos um arquivo pdf que vai armazenar os gráficos 
pdf("/cloud/project/figures/fevd.pdf", paper = "USr", width = 10, height = 10)

# Gráfico com os resultados
plot(fevd.model, main="", xlab = "Horizonte de Previsão", ylab = "Percentual")

dev.off()





# Defasagens máximas
defasagens = 10
# Montar uma "matriz" de gráficos (kxk)
par(mfrow = c(ncol(data), ncol(data)))
# Adicionar na "matriz" de gráficos a CCF de cada série
# contra ela mesma e as demais
for (i in 1:ncol(data)) {
  for (j in 1:ncol(data)) {
    ccf(var.restricted$varresult[[i]]$residuals, var.restricted$varresult[[j]]$residuals, lag.max = defasagens, 
        main = "", ylab = "FCC", xlab = "Defasagens")
    title(paste0(colnames(data)[i], "-", colnames(data)[j]), adj = 0.5, line = 1)
  }
}

# Aqui, usamos a função roots do pacote vars que recebe como opção:
# - x: modelo VAR estimado
# - modulus: retorna o valor absoluto das raízes. Caso contrário, 
# retorna tanto a parte real como a complexa, caso exista.
vars::roots(x = var.restricted, modulus = TRUE)

