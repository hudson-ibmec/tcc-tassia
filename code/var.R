##############################################
#########         PACOTES              #######
##############################################

# install.packages(c("tidyquant","tidyverse","forecast", "magrittr",
# "MTS","vars","urca","seasonal","rprojroot", "deflateBR", "tbl2xts",
# "Quandl", "BETS", "ecoseries"))

# Pacotes para coletar os dados
suppressMessages(require(tidyquant))
suppressMessages(require(Quandl))
suppressMessages(require(BETS))
suppressMessages(require(ecoseries))
suppressMessages(require(quantmod))

# Pacotes para manusear os dados
suppressMessages(require(tidyverse))
suppressMessages(require(tbl2xts))
suppressMessages(require(readxl))

# Pacote para facilitar o uso de arquivos do projeto
suppressMessages(require(rprojroot))

# Pacote para estimar o modelo var
suppressMessages(require(vars))
suppressMessages(require(urca))
suppressMessages(require(MTS))
suppressMessages(require(forecast))
suppressMessages(require(seasonal))

# Pacote para gerar os gráficos
suppressMessages(require(dygraphs))

# Pacote para deflacionar séries
suppressMessages(require(deflateBR))


##############################################
#########         OPÇÕES               #######
##############################################

root = rprojroot::is_rstudio_project 
mydir = root$find_file()   # Encontra o diretório no qual o projeto atual está salvo

##############################################
#########         DADOS                #######
##############################################

####
## DADOS DO EXCEL
####

# Leitura do arquivo csv deixando a função definir o tipo de cada coluna
dados_tcc = read_excel(paste0(mydir,"/data/dados_tcc.xlsx"))

# Retirar NAs
dados_tcc_clean = na.omit(dados_tcc)

# quantidade de observações
n = nrow(dados_tcc_clean)

# retirar a última linha que tem preço nulo
dados_tcc_clean_last = dados_tcc_clean[1:(n-1),]

# série temporal dos preços dos imóveis
precos_imoveis = xts(dados_tcc_clean_last$preco_imoveis, order.by = dados_tcc_clean_last$data)
colnames(precos_imoveis) = "precos_imoveis"

# série temporal do pib
pib = xts(dados_tcc_clean_last$pib, order.by = dados_tcc_clean_last$data)
colnames(pib) = "pib"

# série temporal dos juros
juros = xts(dados_tcc_clean_last$tx_juros2, order.by = dados_tcc_clean_last$data)
colnames(juros) = "juros"

# série temporal da bolsa
bolsa = xts(dados_tcc_clean_last$bolsa, order.by = dados_tcc_clean_last$data)
colnames(bolsa) = "bolsa"

####
## DADOS COLETADOS AUTOMATICAMENTE PELO R
####

from = "2011-02-01"
type = "xts"
quandl_api_key("NjGc22-41R7zD_K7Pt7z")

# Índice IBOVESPA - Mensal e em pontos (BCB) - Obtemos a taxa de variação mensal
bvsp = Quandl::Quandl("BCB/7845", type = type, start_date = from, transform = "rdiff")
colnames(bvsp) = "bvsp"

# PIB acumulado dos últimos 12 meses - Mensal e em valores correntes (R$ milhões) (BCB) - Obtemos a taxa de variação mensal
pibacum12m = Quandl::Quandl("BCB/4382", type = type, start_date = from) %>%
             tbl2xts::xts_tbl() %>%
             dplyr::select(date, pibacum12m = "coredata.xts.") %>%
             dplyr::mutate(date = as.Date(date)) %>%
             dplyr::mutate(pib_deflacionado = deflateBR::deflate(pibacum12m, date, "03/2011", index = "ipca")) %>%
             dplyr::mutate(pibvar = (pib_deflacionado/lag(pib_deflacionado)-1)*100) %>%
             dplyr::select(date, pibvar) %>%
             tbl2xts::tbl_xts(., cols_to_xts = "pibvar")

# Taxa média mensal de juros - Pessoas físicas - Financiamento imobiliário com taxas de mercado
juros_imobiliarios = BETSget(25497, data.frame = T) %>%
                      dplyr::as.tbl() %>%
                      dplyr::select(date, juros_imobiliarios = value) %>%
                      dplyr::mutate(jurosvar = (juros_imobiliarios/lag(juros_imobiliarios)-1)*100) %>%
                      tbl2xts::tbl_xts(., cols_to_xts = "jurosvar")


juros_imobiliarios = juros_imobiliarios[paste0(from,"/")] # selecionar para o período de interesse

juros_imobiliarios2 = BETSget(25498, data.frame = T) %>%
                      dplyr::as.tbl() %>%
                      dplyr::select(date, juros_imobiliarios2 = value) %>%
                      dplyr::mutate(jurosvar2 = (juros_imobiliarios2/lag(juros_imobiliarios2)-1)*100) %>%
                      tbl2xts::tbl_xts(., cols_to_xts = "jurosvar2")

juros_imobiliarios2 = juros_imobiliarios2[paste0(from,"/")] # selecionar para o período de interesse

juros_imobiliarios3 = BETSget(25499, data.frame = T) %>%
                      dplyr::as.tbl() %>%
                      dplyr::select(date, juros_imobiliarios3 = value) %>%
                      dplyr::mutate(jurosvar3 = (juros_imobiliarios3/lag(juros_imobiliarios3)-1)*100) %>%
                      tbl2xts::tbl_xts(., cols_to_xts = "jurosvar3")

juros_imobiliarios3 = juros_imobiliarios3[paste0(from,"/")] # selecionar para o período de interesse

# Índice de valores de garantia de imóveis residenciais financiados (IVG-R) - Em índice
# Estima a tendência de preço de longo prazo dos valores de imóveis residenciais no Brasil
# utilizando informações do Sistema de Informações de Crédito (SCR) do Banco Central do Brasil. 
# Para isso, utiliza os valores de avaliação dos imóveis dados em garantia a financiamentos 
# imobiliários residenciais para pessoas físicas nas modalidades de alienação fiduciária e 
# hipoteca residencial. O cálculo é realizado considerando as mesmas regiões metropolitanas 
# usadas no cálculo do IPCA pelo IBGE: 11 até dezembro de 2013 (Belém, Belo Horizonte, Brasília, 
# Curitiba, Fortaleza, Goiânia, Porto Alegre, Recife, Rio de Janeiro, Salvador e São Paulo), 
# e 13 a partir de janeiro de 2014 (inclui Campo Grande e Vitória, conforme NT/IBGE 03/2013).

ivgr = Quandl::Quandl("BCB/21340", type = type, start_date = from) %>%
       tbl2xts::xts_tbl() %>%
       dplyr::select(date, ivgr = "coredata.xts.") %>%
       dplyr::mutate(date = as.Date(date)) %>%
       dplyr::mutate(ivgrvar = (ivgr/lag(ivgr)-1)*100) %>%
       tbl2xts::tbl_xts(., cols_to_xts = "ivgrvar")

# Mediana dos valores de garantia de imóveis residenciais financiados (MVG-R) - em R$ 
# É uma série temporal complementar ao IVG-R (série 21340). É calculada a partir dos 
# valores de avaliação dos imóveis dados em garantia a financiamentos imobiliários 
# residenciais para pessoas físicas nas modalidades de alienação fiduciária e 
# hipoteca residencial, informados pelos bancos ao Sistema de Informações de Crédito (SCR) 
# do Banco Central do Brasil. Seu cálculo consiste em partir da mesma base de dados 
# para o cálculo do IVG-R e tomar a mediana dos valores de avaliação dos imóveis em uma 
# janela móvel de três meses. A abrangência é nacional e o valor é expresso em reais. 
# Considera como filtro de dados apenas financiamentos com valor contratado superior 
# a um salário mínimo anual, dado como 13,3 vezes o salário mínimo mensal vigente no ano.

mvgr = BETSget(25419, data.frame = T) %>%
       dplyr::as.tbl() %>%
       dplyr::select(date, precos_mvgr = value) %>%
       dplyr::mutate(precos_mvgr_var = (precos_mvgr/lag(precos_mvgr)-1)*100) %>%
       tbl2xts::tbl_xts(., cols_to_xts = "precos_mvgr_var")

mvgr = mvgr[paste0(from,"/")] # selecionar para o período de interesse

##############################################
#######    EXPLORATORY DATA ANALYSIS   #######
##############################################

# união dos dados em um único objeto
data = xts::merge.xts(bvsp, pibacum12m, juros_imobiliarios, juros_imobiliarios2,
                         juros_imobiliarios3, ivgr, mvgr)
data = na.omit(data)
data_names = colnames(data)

# Estatística descritiva das séries 
statNames = c("Média", "Desvio Padrão", "Assimetria", "Curtose", "Tamanho")
series_stats_data = matrix(data = NA, nrow = length(data_names), ncol = length(statNames))
colnames(series_stats_data) = statNames

for (i in 1:nrow(series_stats_data)) {
  series_stats_data[i,1] = round(mean(data[,i], na.rm = TRUE),3)
  series_stats_data[i,2] = round(sd(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,3] = round(skewness(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,4] = round(kurtosis(data[,i], na.rm = TRUE), 3)
  series_stats_data[i,5] = length(na.omit(data[,i]))
}

# Mostrar a tabela com as estatísticas descritivas
print(series_stats_data)

# Gráfico das séries temporais - agrupado com 4 séries por tipo
par(mfrow=c(2,2))
plot(data$bvsp, xlab = "", ylab = "", type = "l", main = "Taxa de variação do IBOVESPA - %", cex = 0.5)
plot(data$pibvar, xlab = "", ylab = "", type = "l", main = "Taxa de variação do PIB Real - %", cex = 0.5)
plot(data$jurosvar, xlab = "", ylab = "", type = "l", main = "Taxa variação dos Juros - %", cex = 0.5)
plot(data$jurosvar2, xlab = "", ylab = "", type = "l", main = "Taxa variação dos Juros - %", cex = 0.5)

# Gráficos das taxas de juros para comparativo
par(mfrow=c(3,1))
plot(data$jurosvar3, xlab = "", ylab = "", type = "l", main = "Taxa de variação dos Juros - %", cex = 0.5)
plot(data$ivgrvar, xlab = "", ylab = "", type = "l", main = "Taxa de variação IVG - %", cex = 0.5)
plot(data$precos_mvgr_var, xlab = "", ylab = "", type = "l", main = "Taxa de variação MVG - %", cex = 0.5)

##############################################
#######          MODELO VAR            #######
##############################################

# Continuar daqui usando a base de dados vardata
vardata = data 

